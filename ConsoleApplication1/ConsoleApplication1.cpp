#include "stdafx.h"
#include <iostream>
#include <Winbase.h>
#include <Windows.h>

using namespace std;

void main()
{
	setlocale(LC_ALL, "Russian");
	int a;
	STARTUPINFO cif;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	PROCESS_INFORMATION pi;
	if (CreateProcess("c:\\windows\\notepad.exe", NULL, NULL, NULL, FALSE, NULL, NULL, NULL, &cif, &pi) == TRUE)
	{
		cout << "Введите требуемый приоритет процесса: " << "1 - Реального времени\n" << "2 - Высокий\n" << "3 - Выше среднего\n" << "4- Средний\n" << 
			"5 - Ниже среднего\n"  << "6 - пустой\n" << endl;
		cin >> "Приоритет: " >> a;
		switch (a)
		{
		case 1: SetPriorityClass(pi.hProcess, REALTIME_PRIORITY_CLASS); break;
		case 2: SetPriorityClass(pi.hProcess, HIGH_PRIORITY_CLASS); break;
		case 3: SetPriorityClass(pi.hProcess, ABOVE_NORMAL_PRIORITY_CLASS); break;
		case 4: SetPriorityClass(pi.hProcess, NORMAL_PRIORITY_CLASS); break;
		case 5: SetPriorityClass(pi.hProcess, BELOW_NORMAL_PRIORITY_CLASS); break;
		case 6: SetPriorityClass(pi.hProcess, IDLE_PRIORITY_CLASS); break;
		}
		cout << "процесс" << endl;
		cout << "дескриптор" << pi.hProcess << endl;
		cout << "поток: " << pi.hThread << endl;
		TerminateProcess(pi.hProcess, NO_ERROR);
	}
	system("PAUSE");
}
